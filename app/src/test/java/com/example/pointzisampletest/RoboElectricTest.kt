package com.example.pointzisampletest




import android.content.Intent
import android.os.Looper.getMainLooper
import android.widget.Button
import android.widget.TextView
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.Shadows
import org.robolectric.Shadows.shadowOf
import org.robolectric.android.controller.ActivityController
import org.robolectric.annotation.Config
import org.robolectric.shadows.ShadowActivity
import org.robolectric.shadows.ShadowIntent


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(RobolectricTestRunner::class)
@Config( sdk = [29], packageName = "com.example.pointzisampletest")

class RoboElectricTest {
    private lateinit var controller: ActivityController<HomeActivity>
    private lateinit var mainActivity:MainActivity
    private lateinit var homeActivity: HomeActivity
    //Pontzi ids
    // com.pointzi.FEED_ID
    // com.pointzi.INSTALL_ID

    @Before
    fun init(){
        shadowOf(getMainLooper()).idle()
        mainActivity = Robolectric.setupActivity(MainActivity::class.java)
        homeActivity = Robolectric.setupActivity(HomeActivity::class.java)


    }



    @Test
    fun checkTextViewString_presentOrNot(){

        mainActivity = Robolectric.setupActivity(MainActivity::class.java)



        //val textView=mainActivity.findViewById<TextView>(com.pointzi.R.id.pz_titleTextView)
        val textView=mainActivity.findViewById<TextView>(R.id.mainTextView)

        assertNotNull(textView);
        val stringValue=textView.text.toString()
        assertThat(stringValue,equalTo("Hello Pointzi!"))
    }

    @Test
    fun clickingLoginButton_shouldStartSecondActivity(){
        val button= mainActivity.findViewById<Button>(R.id.button)

        assertNotNull(button);
        button.performClick()
        val showActivity:ShadowActivity= Shadows.shadowOf(mainActivity)
        val intent:Intent=showActivity.nextStartedActivity
        val shadowIntent:ShadowIntent=shadowOf(intent)
        assertThat(shadowIntent.intentClass.name, equalTo(HomeActivity::class.java!!.getName()))

    }

    @Test
    @Config(qualifiers = "+port")
    fun testOrientationChange() {
        controller = Robolectric.buildActivity(HomeActivity::class.java)
        controller.setup()
        // assert that activity is in portrait mode
        RuntimeEnvironment.setQualifiers("+land")
        controller.configurationChange()


        // assert that activity is in landscape mode
        val textView=mainActivity.findViewById<TextView>(com.pointzi.R.id.pz_titleTextView)
        val stringValue=textView.text.toString()
        assertThat(stringValue,equalTo("Material Design Tip"))
    }





}