package com.example.pointzisampletest

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.pointzi.Pointzi


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }

    fun buttonClickTest(view: View){
        val intent= Intent(applicationContext, HomeActivity::class.java)
        startActivity(intent)
    }
}